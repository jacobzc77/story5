from django.urls import path

from . import views

app_name = 'websitegue'
urlpatterns = [
    path('', views.index, name="Home"),
    path('profile/',views.profile, name="Profile"),
    path('experiences/',views.experiences, name="Experiences"),
    path('skills/',views.skills, name="Skills"),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete'),
]
